﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using UnityEngine;

namespace LOGMultiplayer
{
    [KSPAddon(KSPAddon.Startup.EveryScene, false)]
    public class LOGMain : MonoBehaviour
    {
        public static string KSPDirectory = Environment.CurrentDirectory.ToString();
        public static Rect WindowsPosition;
        public struct Player
        {
            public string RecvIP;// = "";         //Recivied IP
            public float RecvX;// = 0f;           //Recivied X pos
            public float RecvY;// = 0f;           //Recivied Y pos
            public float RecvZ;// = 0f;           //Recivied Z pos
            public float RecvRootX; // = 0f;
            public float RecvRootY; // = 0f;
            public float RecvRootZ; // = 0f;
            public float RecvRotW; //= 0f;
            public float RecvRotX; //= 0f;
            public float RecvRotY; //= 0f;
            public float RecvRotZ; //= 0f;
            public Vessel vessel;
        };
        public static Player[][] player = new Player[2][];
        public static void Connect()
        {
            ClientConnections.ConnectToServer();
        }
        public static void Disconnect()
        {
            ClientConnections.CloseAllConnections();
        }
        public void Update()
        {
            ClientVesselUpdate.UpdateVessel();

        }
        private static void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Debug.LogError("Ping failed:");
                Debug.LogError(e.Error.ToString());
            }
        }
        public void Main()
        {
            ClientOther.ReadFile(KSPDirectory +"/GameData/L.O.G/TakedIP.tmp");
            ClientConnections.ServerIPAddress = ClientOther.TempText;
            string IPText = ClientConnections.ServerIPAddress;

            WindowsPosition = new Rect(Screen.width / 2, Screen.height / 2, 10, 10);
            RenderingManager.AddToPostDrawQueue(3, new Callback(ClientGUI.OnGUI));

            ClientConnections.ServerIPAddressForEdit = string.Format(IPText);
            ClientConnections.PortForEdit = string.Format("4198");

            /*if (HighLogic.LoadedScene == GameScenes.MAINMENU)
            {
                ClientConnections.ConnectToServer();
            }*/

            //ClientOther.DeleteFile("TakedIP.tmp");

            for (int i = 0; i < ClientVesselUpdate.playerCount; i++)
            {
                for(int d = 0; d < ClientVesselUpdate.playerCount; d++)
                {
                    player[i][d].RecvIP = "";
                    player[i][d].RecvRootX = 0f;
                    player[i][d].RecvRootY = 0f;
                    player[i][d].RecvRootZ = 0f;
                    player[i][d].RecvX = 0f;
                    player[i][d].RecvY = 0f;
                    player[i][d].RecvZ = 0f;
                    player[i][d].RecvRotW = 0f;
                    player[i][d].RecvRotX = 0f;
                    player[i][d].RecvRotY = 0f;
                    player[i][d].RecvRotZ = 0f;
                    player[i][d].vessel = null;
                }
            }
        }
    }
}
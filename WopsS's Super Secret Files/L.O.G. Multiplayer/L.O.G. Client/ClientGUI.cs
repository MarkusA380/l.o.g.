﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace LOGMultiplayer
{
    class ClientGUI : MonoBehaviour
    {
        public static void ConnectGUI(int windowID)
        {
            GUIStyle WindowStyle = new GUIStyle(GUI.skin.button);
            WindowStyle.normal.textColor = WindowStyle.focused.textColor = Color.white;
            WindowStyle.hover.textColor = WindowStyle.active.textColor = Color.yellow;
            WindowStyle.onNormal.textColor = WindowStyle.onFocused.textColor = WindowStyle.onHover.textColor = WindowStyle.onActive.textColor = Color.green;
            WindowStyle.padding = new RectOffset(2, 2, 2, 2);

            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            GUILayout.Label("IP:", GUILayout.MinWidth(50));
            ClientConnections.ServerIPAddressForEdit = GUILayout.TextField(ClientConnections.ServerIPAddressForEdit, 15, GUILayout.MinWidth(90));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Port:", GUILayout.MinWidth(50));
            ClientConnections.PortForEdit = GUILayout.TextField(ClientConnections.PortForEdit, 15, GUILayout.MinWidth(90));
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Connect", WindowStyle, GUILayout.ExpandWidth(true)))
            {
                ClientConnections.ConnectToServer();
                OnGUI();
            }

            GUILayout.EndVertical();
            GUI.DragWindow(new Rect(0, 0, 10000, 20));
        }

        public static void ConnectedGUI(int windowID)
        {
            GUIStyle WindowStyle = new GUIStyle(GUI.skin.button);
            WindowStyle.normal.textColor = WindowStyle.focused.textColor = Color.white;
            WindowStyle.hover.textColor = WindowStyle.active.textColor = Color.yellow;
            WindowStyle.onNormal.textColor = WindowStyle.onFocused.textColor = WindowStyle.onHover.textColor = WindowStyle.onActive.textColor = Color.green;
            WindowStyle.padding = new RectOffset(2, 2, 2, 2);

            GUILayout.BeginVertical();

            if (GUILayout.Button("Disconnect", WindowStyle, GUILayout.ExpandWidth(true)))
            {
                ClientConnections.CloseAllConnections();
                OnGUI();
                ClientConnections.IsConnected = false;
            }

            GUILayout.EndVertical();
            GUI.DragWindow(new Rect(0, 0, 10000, 20));
        }

        public static void OnGUI()
        {
            GUI.skin = HighLogic.Skin;
            if (ClientConnections.IsConnected == false)
            {
                LOGMain.WindowsPosition = GUILayout.Window(1, LOGMain.WindowsPosition, ConnectGUI, "L.O.G. Connect", GUILayout.MinWidth(200));
            }
            else
            {
                LOGMain.WindowsPosition = GUILayout.Window(1, LOGMain.WindowsPosition, ConnectedGUI, "L.O.G.", GUILayout.MinWidth(200));
            }
        }
    }
}
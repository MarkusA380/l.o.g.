﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace LOGMultiplayer
{
    public class ClientVesselConvert
    {
        public static string ConvertVessel(Vessel vessel)
        {
            try
            {
                StringBuilder VesselBuldier = new StringBuilder();
                VesselBuldier.Append(vessel.GetWorldPos3D().x.ToString()).Append(" ");
                VesselBuldier.Append(vessel.GetWorldPos3D().y.ToString()).Append(" ");
                VesselBuldier.Append(vessel.GetWorldPos3D().z.ToString()).Append(" ");
                VesselBuldier.Append(FlightGlobals.ActiveVessel.mainBody.position.x.ToString()).Append(" ");
                VesselBuldier.Append(FlightGlobals.ActiveVessel.mainBody.position.y.ToString()).Append(" ");
                VesselBuldier.Append(FlightGlobals.ActiveVessel.mainBody.position.z.ToString()).Append(" ");
                VesselBuldier.Append(vessel.transform.rotation.x.ToString()).Append(" ");
                VesselBuldier.Append(vessel.transform.rotation.y.ToString()).Append(" ");
                VesselBuldier.Append(vessel.transform.rotation.z.ToString()).Append(" ");
                VesselBuldier.Append(vessel.transform.rotation.w.ToString());

                return VesselBuldier.ToString();
            }
            catch (Exception ex)
            {
                Debug.LogError("L.O.G.: " + ex.Message);
            }
            return "";
        }
    }
}
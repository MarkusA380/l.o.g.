﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace LOGMultiplayer
{
    public class ClientConnections
    {
        public static Socket ClientSocket;

        public static string ServerIPAddress = "";
        public static string ExternalIP = "";
        public static string LocalIP = "";

        public static string ServerIPAddressForEdit;
        public static string PortForEdit;

        public static string Port = "4198";
        public static int PortForConnect;

        public static bool IsConnected = false;
        public static bool IsAnotherPlayerConnected = false;
        public static void ConnectToServer()
        {
            ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            int ConnectionAttempts = 0;

            while (!ClientSocket.Connected && ConnectionAttempts != 1)
            {
                if (IsConnected == false)
                {
                    try
                    {
                        IsConnected = true;

                        string IPAddressForConnect = ClientConnections.ServerIPAddress;
                        PortForConnect = Convert.ToInt32(Port);

                        ClientLOG.LOGText("Trying to connect to the server.");
                        ClientSocket.Connect(IPAddressForConnect, PortForConnect);
                    }
                    catch (SocketException) { }
                    if (!ClientSocket.Connected)
                    {
                        ConnectionAttempts++;
                        ClientLOG.LOGText("Trying to reconnect to the server.");
                    }
                }
            }
            if (ConnectionAttempts == 1)
            {
                ClientLOG.LOGText("Connection time out.");
                ClientLOG.LOGTextWithoutDebugDisplay("-------------------------------------");
                ClientLOG.LOGTextWithoutDebugDisplay("");
                CloseAllConnections();
                ConnectionAttempts = 0;
            }

            if (ClientSocket.Connected)
            {
                ClientLOG.LOGTextWithoutDebugDisplay("");
                ClientLOG.LOGText("Connected to server.");
                ClientLOG.LOGTextWithoutDebugDisplay("-------------------------------------");

                ClientOther.StartGame();
            }
        }
        public static void KeepConnected()
        {
            ClientLOG.LOGText("Type /quit to disconnect from the server.");

            while (true)
            {
                ClientSockets.BytesToSend();
                ClientSockets.ReceivedBytes();
            }
        }
        public static void CloseAllConnections()
        {
            ClientConnections.IsAnotherPlayerConnected = false;
            ClientLOG.LOGTextWithoutDebugDisplay("");
            ClientLOG.LOGTextWithoutDebugDisplay("------------------Disconnect attempt at " + DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + "-------------------");
            ClientLOG.LOGText("Try to disconnect the server.");
            try
            {
                ClientSockets.SendedBytes("/quit");
                ClientSocket.Shutdown(SocketShutdown.Both);
                ClientSocket.Close();
                ClientConnections.IsConnected = false;
            }
            catch
            {
                ClientSocket.Close();
                ClientConnections.IsConnected = false;
            }
            ClientLOG.LOGText("Disconnected.");
            ClientLOG.LOGTextWithoutDebugDisplay("---------------------------------------------------------------");
        }
        public static void GetExternalIP()
        {
            string IP = Dns.GetHostName();
            IPHostEntry IPEntry = Dns.GetHostEntry(IP);
            IPAddress[] IPAddress = IPEntry.AddressList;
            for (int i = 0; i < IPAddress.Length; i++)
            {
                if (IPAddress[i].AddressFamily == AddressFamily.InterNetwork)
                {
                    ExternalIP = IPAddress[0].ToString();
                }
            }
        }
        public static void GetLocalIP()
        {
            IPHostEntry PlayerIPAddress;
            PlayerIPAddress = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress IP in PlayerIPAddress.AddressList)
            {
                if (IP.AddressFamily == AddressFamily.InterNetwork)
                {
                    LocalIP = IP.ToString();
                }
            }
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace LOGMultiplayer
{
    class ClientVesselBuild : MonoBehaviour
    {
        public static Vessel PlayerVessel;
        public static Vessel Player_Vessel = new Vessel();
        public static Vector3 PrebuildedPosition = new Vector3();

        public static string path = LOGMain.KSPDirectory + "/Ships/VAB/Rover + Skycrane.craft";

        public static float SpawnHeightOffset = 1.0f;
        public static void BuildVessel()
        {
            try
            {
                for (int i = 1; i <= ClientVesselUpdate.playerCount; i++)
                {

                    ShipConstruct LoadedShip = ShipConstruction.LoadShip(ClientVesselBuild.path);

                    Vector3 offset = Vector3.up * ClientVesselBuild.SpawnHeightOffset;

                    Transform TransformPlayerVessel = ClientVesselBuild.PlayerVessel.transform;

                    TransformPlayerVessel.position = new Vector3(5f, 5f, 5f);

                    string landedAt = "Launchpad";
                    string flag = "default";
                    Game state = FlightDriver.FlightStateCache;
                    VesselCrewManifest crew = new VesselCrewManifest();

                    GameObject LaunchPosition = new GameObject();
                    LaunchPosition.transform.position = TransformPlayerVessel.position;
                    LaunchPosition.transform.position += TransformPlayerVessel.TransformDirection(offset);
                    LaunchPosition.transform.rotation = TransformPlayerVessel.rotation;
                    ShipConstruction.CreateBackup(LoadedShip);
                    ShipConstruction.PutShipToGround(LoadedShip, LaunchPosition.transform);
                    Destroy(LaunchPosition);
                    ShipConstruction.AssembleForLaunch(LoadedShip, landedAt, flag, state, crew);
                    FlightGlobals.SetActiveVessel(ClientVesselBuild.PlayerVessel);
                    ClientConnections.IsAnotherPlayerConnected = true;
                }
            }
            catch(Exception)
            {
                Debug.LogWarning("Is in ClientVesselBuild.");
            }
        }
    }
}
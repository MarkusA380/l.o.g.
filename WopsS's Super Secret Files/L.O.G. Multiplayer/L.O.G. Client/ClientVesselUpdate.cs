﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace LOGMultiplayer
{
    class ClientVesselUpdate : MonoBehaviour
    {
        public static string[] messageParts; //The parts of a message
        public static string[] messageArray; //The array of the messages
        public static string[] messagePre; //One half the message Arrray, other one the playercount
        public static int playerCount;

        public static string vesselData = "";
        public static void UpdateVessel()
        {
            if (HighLogic.LoadedScene == GameScenes.FLIGHT)
            {
                try
                {
                    int i;
                    string[] ip = new string[2];

                    ClientOther.Cheats();

                    string vesselData = "";           //Data to be sent
                    string RecvMsg = String.Empty;

                    ClientVesselBuild.PlayerVessel = FlightGlobals.ActiveVessel;
                    ClientVesselBuild.Player_Vessel = FlightGlobals.FindNearestControllableVessel(ClientVesselBuild.PlayerVessel);

                    if (ClientConnections.IsConnected == true)
                    {
                        if (FlightGlobals.ActiveVessel == null)
                        {
                            Debug.LogError("L.O.G.: Active vessel was null.");
                            return;
                        }
                        try
                        {
                            ClientVesselBuild.PlayerVessel = FlightGlobals.ActiveVessel;

                            ClientVesselBuild.Player_Vessel = FlightGlobals.FindNearestControllableVessel(ClientVesselBuild.PlayerVessel);
                        }
                        catch (Exception)
                        {
                            Debug.LogWarning("Here is the virus.");
                        }
                        try
                        {
                            vesselData = ClientVesselConvert.ConvertVessel(ClientVesselBuild.PlayerVessel);

                            byte[] BytesToSend = new ASCIIEncoding().GetBytes(vesselData);
                            ClientConnections.ClientSocket.Send(BytesToSend, 0, BytesToSend.Length, SocketFlags.None);

                            byte[] BytesReceived = new byte[3000];
                            int BytesNumber = ClientConnections.ClientSocket.Receive(BytesReceived, BytesReceived.Length, 0);

                            for (int b = 0; b < BytesNumber; b++)
                            {
                                RecvMsg = RecvMsg + (Convert.ToChar(BytesReceived[b]));
                            }
                        }
                        catch (Exception)
                        {
                            Debug.LogWarning("No, no. Here is the virus.");
                        }
                        try
                        {
                            try
                            {
                                messagePre = RecvMsg.Split('#');
                                //messageArray = messagePre[1].Split('$');
                            }
                            catch (Exception)
                            {
                                Debug.LogWarning("First 2 lines, from the error.");
                            }
                            try
                            {
                                playerCount = Int16.Parse(messagePre[0], System.Globalization.CultureInfo.InvariantCulture);
                                messageParts = messagePre[1].Split(' ');
                            }
                            catch (Exception)
                            {
                                Debug.LogWarning("Player Count!");
                            }
                            try
                            {
                                if (messageParts.Length != 11)
                                {
                                    Debug.LogError("LOGclient: Received invalid message <" + RecvMsg + ">");
                                    return;
                                }
                            }
                            catch (Exception)
                            {
                                Debug.LogWarning("Kidding me?");
                            }
                        }
                        catch (Exception)
                        {
                            Debug.LogWarning("That was!");
                        }
                        //Debug.Log("#LOGclient 6");
                        try
                        {
                            for (i = 0; i < playerCount; i++)
                            {
                                for (int d = 0; d < playerCount; d++)
                                {
                                    Debug.LogError(i);
                                    LOGMain.player[i][d].RecvIP = messageParts[0];
                                    Debug.Log("#LOGclient RecvIP: " + LOGMain.player[i][d].RecvIP);
                                    LOGMain.player[i][d].RecvX = (float)double.Parse(messageParts[1], System.Globalization.CultureInfo.InvariantCulture);
                                    LOGMain.player[i][d].RecvY = (float)double.Parse(messageParts[2], System.Globalization.CultureInfo.InvariantCulture);
                                    LOGMain.player[i][d].RecvZ = (float)double.Parse(messageParts[3], System.Globalization.CultureInfo.InvariantCulture);
                                    LOGMain.player[i][d].RecvRootX = (float)double.Parse(messageParts[4], System.Globalization.CultureInfo.InvariantCulture);
                                    LOGMain.player[i][d].RecvRootY = (float)double.Parse(messageParts[5], System.Globalization.CultureInfo.InvariantCulture);
                                    LOGMain.player[i][d].RecvRootZ = (float)double.Parse(messageParts[6], System.Globalization.CultureInfo.InvariantCulture);
                                    LOGMain.player[i][d].RecvRotX = (float)double.Parse(messageParts[7], System.Globalization.CultureInfo.InvariantCulture);
                                    LOGMain.player[i][d].RecvRotY = (float)double.Parse(messageParts[8], System.Globalization.CultureInfo.InvariantCulture);
                                    LOGMain.player[i][d].RecvRotZ = (float)double.Parse(messageParts[9], System.Globalization.CultureInfo.InvariantCulture);
                                    LOGMain.player[i][d].RecvRotW = (float)double.Parse(messageParts[10], System.Globalization.CultureInfo.InvariantCulture);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            Debug.LogWarning("Is here the error.");
                        }
                        ClientConnections.GetExternalIP();
                        ClientConnections.GetLocalIP();
                        //Debug.Log("#LOGclient 7");
                        try
                        {
                            for (i = 0; i < playerCount; i++)
                            {
                                for (int d = 0; d < playerCount; d++)
                                {
                                    try
                                    {
                                        ip = LOGMain.player[i][d].RecvIP.Split(':');
                                        try
                                        {
                                            if (ip[0] != ClientConnections.LocalIP && ip[0] != ClientConnections.ExternalIP)
                                            {
                                                Debug.LogWarning("IP without port, " + ip[0] + " local IP " + ClientConnections.LocalIP);
                                                try
                                                {
                                                    if (LOGMain.player[i][d].vessel == null)
                                                    {
                                                        try
                                                        {
                                                            ClientVesselBuild.PrebuildedPosition = ClientVesselBuild.PlayerVessel.GetWorldPos3D();
                                                            ClientVesselBuild.PlayerVessel.GoOnRails();
                                                            ClientVesselBuild.BuildVessel();
                                                            ClientVesselBuild.PlayerVessel.GoOffRails();
                                                        }
                                                        catch (Exception)
                                                        {
                                                            Debug.LogWarning("Is in second IF-statement.");
                                                        }
                                                        try
                                                        {
                                                            ClientVesselBuild.PlayerVessel = FlightGlobals.ActiveVessel;
                                                            LOGMain.player[i][d].vessel = FlightGlobals.FindNearestControllableVessel(ClientVesselBuild.PlayerVessel); //after building, the vessels have to be set new.
                                                            LOGMain.player[i][d].vessel.SetPosition(ClientVesselBuild.PlayerVessel.mainBody.position - new Vector3d(LOGMain.player[i][d].RecvRootX, LOGMain.player[i][d].RecvRootY, LOGMain.player[i][d].RecvRootZ) + new Vector3d(LOGMain.player[i][d].RecvX, LOGMain.player[i][d].RecvY, LOGMain.player[i][d].RecvZ));
                                                            LOGMain.player[i][d].vessel.SetWorldVelocity(new Vector3d(0, 0, 0));
                                                            ClientVesselBuild.PlayerVessel.SetWorldVelocity(new Vector3d(0, 0, 0));
                                                            ClientVesselBuild.PlayerVessel.SetPosition(ClientVesselBuild.PrebuildedPosition);
                                                        }
                                                        catch (Exception)
                                                        {
                                                            Debug.LogWarning("Is in second IF-statement, after first.");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            LOGMain.player[i][d].vessel.SetPosition(ClientVesselBuild.PlayerVessel.mainBody.position - new Vector3d(LOGMain.player[i][d].RecvRootX, LOGMain.player[i][d].RecvRootY, LOGMain.player[i][d].RecvRootZ) + new Vector3d(LOGMain.player[i][d].RecvX, LOGMain.player[i][d].RecvY, LOGMain.player[i][d].RecvZ));
                                                            print(LOGMain.player[i][d].RecvX + " " + LOGMain.player[i][d].RecvY + " " + LOGMain.player[i][d].RecvZ);
                                                            LOGMain.player[i][d].vessel.SetRotation(new Quaternion(LOGMain.player[i][d].RecvRotX, LOGMain.player[i][d].RecvRotY, LOGMain.player[i][d].RecvRotZ, LOGMain.player[i][d].RecvRotW));
                                                        }
                                                        catch (Exception)
                                                        {
                                                            Debug.LogWarning("Is in second IF-statement -> else-statement");
                                                        }
                                                    }
                                                }
                                                catch (Exception)
                                                {
                                                    Debug.LogWarning("Is after the first IF-statement.");
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            Debug.LogWarning("Is above the first IF-statement.");
                                        }
                                        //Debug.Log("#LOGclient 8");
                                        try
                                        {
                                            if (LOGMain.player[i][d].vessel != null)
                                            {
                                                LOGMain.player[i][d].vessel.SetWorldVelocity(new Vector3d(0, 0, 0));
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            Debug.LogWarning("Is in \'LOGMain.player[i][d].vessel != null\'");
                                        }

                                    }
                                    catch (Exception)
                                    {
                                        Debug.LogWarning("Here it is.");
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            Debug.LogWarning("Yes, is here.");
                        }
                    }
                }
                catch (Exception)
                {
                    Debug.LogWarning("Hmmm.");
                }
            }
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace LOGMultiplayer
{
    class ClientOther
    {
        public static string TempText;
        public static void Cheats()
        {
            CheatOptions.InfiniteEVAFuel = false;
            CheatOptions.InfiniteFuel = false;
            CheatOptions.InfiniteRCS = false;
            CheatOptions.NoCrashDamage = false;
            CheatOptions.UnbreakableJoints = false;
        }
        public static void StartGame()
        {
            try
            {
                HighLogic.SaveFolder = "LOG";
                HighLogic.CurrentGame = GamePersistence.LoadGame("persistent", HighLogic.SaveFolder, false, true);
                HighLogic.CurrentGame.Title = "L.O.G.";
                HighLogic.CurrentGame.Description = "L.O.G. - Multiplayer";
                HighLogic.CurrentGame.Parameters.Flight.CanAutoSave = false;
                HighLogic.CurrentGame.Parameters.Flight.CanLeaveToEditor = false;
                HighLogic.CurrentGame.Parameters.Flight.CanLeaveToMainMenu = false;
                HighLogic.CurrentGame.Parameters.Flight.CanQuickLoad = false;
                HighLogic.CurrentGame.Parameters.Flight.CanRestart = false;
                HighLogic.CurrentGame.Parameters.Flight.CanTimeWarpLow = false;
                HighLogic.CurrentGame.Parameters.Flight.CanTimeWarpHigh = false;
                HighLogic.CurrentGame.Parameters.Flight.CanSwitchVesselsFar = false;
                //ClientOther.Cheats();
                GamePersistence.SaveGame("persistent", HighLogic.SaveFolder, SaveMode.OVERWRITE);
            }
            catch (Exception)
            {
                Debug.Log("Aici este");
            }
            try
            {
                MapView.EnterMapView();
                MapView.MapCamera.SetTarget("Kerbin");
                HighLogic.CurrentGame.Start();
            }
            catch (Exception)
            {
                Debug.Log("Aici e.");
            }
        }
        public static void ReadFile(string filename)
        {
            TempText = File.ReadAllText(filename);      
        }
        public static void DeleteFile(string filename)
        {
            File.Delete(filename);
        }
    }
}

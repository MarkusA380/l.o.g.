﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;

namespace LOGLauncher
{
    public partial class Main : Form
    {
        public static string KSPDirectory = Environment.CurrentDirectory.ToString();
        public static string FilePath = KSPDirectory + "\\GameData\\L.O.G\\TakedIP.tmp";

        public static string ServerIP;
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            LoadServers();
        }

        private void Refresh_Button_Click(object sender, EventArgs e)
        {
            LoadServers();
        }
        private void LoadServers()
        {
            Stream FileDirectory = File.Open(KSPDirectory + "\\GameData\\L.O.G\\L.O.G.data", FileMode.Open, FileAccess.ReadWrite);
            if (File.Exists(KSPDirectory + "\\GameData\\L.O.G\\L.O.G.data"))
            {
                StreamReader Reader = new StreamReader(FileDirectory);
                string[] columnnames = Reader.ReadLine().Split('|');
                DataTable dt = new DataTable();
                foreach (string c in columnnames)
                {
                    dt.Columns.Add(c);
                }
                string newline;
                while ((newline = Reader.ReadLine()) != null)
                {
                    DataRow dr = dt.NewRow();
                    string[] values = newline.Split('|');
                    for (int i = 0; i < values.Length; i++)
                    {
                        dr[i] = values[i];
                    }
                    dt.Rows.Add(dr);
                }
                Reader.Close();
                ListOfServers.DataSource = dt;
            }
            else
            {
                string FirstUse = string.Format("IP|Description");
                File.AppendAllText(KSPDirectory + "\\GameData\\L.O.G\\L.O.G.data", FirstUse + Environment.NewLine);
            }
        }
        public void ListOfServers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //LoadServers();
        }
        public void Play_Button_Click(object sender, EventArgs e)
        {
            int rowindex = ListOfServers.CurrentCell.RowIndex;
            int columnindex = ListOfServers.CurrentCell.ColumnIndex;
            Main.ServerIP = ListOfServers.Rows[rowindex].Cells[0].Value.ToString();
            if (Main.ServerIP.Length > 0)
            {
                WriteFile(Main.ServerIP);
                Process.Start(KSPDirectory +"\\KSP.exe");
            }
        }
        public static void WriteFile(string format)
        {
            if (!File.Exists(FilePath))
            {
                string LogText = string.Format("{0}", string.Format(format));

                File.AppendAllText(FilePath, LogText);
            }
            else
            {
                File.Delete(FilePath);

                string LogText = string.Format("{0}", string.Format(format));

                File.AppendAllText(FilePath, LogText);
            }
        }

        private void Add_Button_Click(object sender, EventArgs e)
        {
            Add add = new Add();
            add.Show();
        }

        private void Delete_Button_Click(object sender, EventArgs e)
        {
            int rowindex = ListOfServers.CurrentCell.RowIndex;
            int LineNumberForDelete = rowindex + 2;
            int LineNumber = 0;
            string line = null;
            Stream FileDirectory = File.Open(KSPDirectory + "\\GameData\\L.O.G\\L.O.G.data", FileMode.Open, FileAccess.ReadWrite);
            using (StreamReader Read = new StreamReader(FileDirectory))
            {
                using (StreamWriter Write = new StreamWriter(FileDirectory))
                {
                    while ((line = Read.ReadLine()) != null)
                    {
                        LineNumber++;

                        if (LineNumber == LineNumberForDelete)
                            continue;

                        FileDirectory.SetLength(0);
                        Write.WriteLine(line);
                    }
                    Write.Close();
                }
                Read.Close();
            }
            LoadServers();
        }
    }
}

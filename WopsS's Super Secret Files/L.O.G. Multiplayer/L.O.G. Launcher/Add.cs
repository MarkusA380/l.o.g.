﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LOGLauncher
{
    public partial class Add : Form
    {
        public static string FileName = Main.KSPDirectory + "\\GameData\\L.O.G\\L.O.G.data";
        public Add()
        {
            InitializeComponent();
        }
        private void AddServer_Button_Click(object sender, EventArgs e)
        {
            if (File.Exists(Main.KSPDirectory + "\\GameData\\L.O.G\\L.O.G.data"))
            {
                if (ServerDescription_Text.TextLength > 0 && ServerIP_Text.TextLength > 0)
                {
                    string AddServer = string.Format("{0}|{1}", string.Format(ServerIP_Text.Text), string.Format(ServerDescription_Text.Text));

                    File.AppendAllText(FileName, AddServer + Environment.NewLine);

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Please fill all boxes.");
                }
            }
            else
            {
                if (ServerDescription_Text.TextLength > 0 && ServerIP_Text.TextLength > 0)
                {
                    string FirstUse = string.Format("IP|Description");
                    File.AppendAllText(FileName, FirstUse + Environment.NewLine);

                    string LogText = string.Format("{0}|{1}", string.Format(ServerIP_Text.Text), string.Format(ServerDescription_Text.Text));
                    File.AppendAllText(FileName, LogText + Environment.NewLine);

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Please fill all boxes.");
                }
            }
        }

        private void Cancel_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

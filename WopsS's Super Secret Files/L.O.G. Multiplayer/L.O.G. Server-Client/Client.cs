﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace L.O.G.Server_Client
{
    class Client
    {
        static void Main(string[] args)
        {
            Console.Title = "L.O.G. - Client";

            ClientLOG.LOGText("Client started at " + DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + ".");

            ClientLOG.TextToSend("Type server IP: ");
            ClientConnections.ServerIP = Console.ReadLine();
            ClientLOG.TextToLog(ClientConnections.ServerIP);

            ClientLOG.TextToSend("Type server port: ");
            ClientConnections.ServerPort = Console.ReadLine();
            ClientLOG.TextToLog(ClientConnections.ServerPort);

            ClientLOG.CheckLogFile();
            ClientConnections.ConnectToServer();
            ClientConnections.KeepConnected();
        }
    }
}

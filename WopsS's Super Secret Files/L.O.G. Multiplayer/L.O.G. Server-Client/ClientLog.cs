﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.O.G.Server_Client
{
    class ClientLOG
    {
        private static string LogFileName = "client_log.txt";

        public static void CheckLogFile()
        {
            if (!System.IO.File.Exists(LogFileName))
            {
                using (System.IO.FileStream fs = System.IO.File.Create(LogFileName)) { }
            }
            if (System.IO.File.Exists(LogFileName))
            {
                TextToLog("");
                LOGText("-------------------------------------");
                LOGText("Log file (client_log.txt) loaded.");
                LOGText("-------------------------------------");
            }
        }
        public static void WriteToLogFile(string format)
        {

            string LogText = string.Format("[{0}]: {1}", DateTime.Now.ToString("dd/MM/yy HH:mm:ss"), string.Format(format));
            string LogTextToConsole = string.Format("{0}", string.Format(format));
            Console.WriteLine(LogTextToConsole);

            File.AppendAllText(LogFileName, LogText + Environment.NewLine);
        }
        public static void LOGText(string format)
        {
            WriteToLogFile(format);
        }
        public static void TextToLog(string format)
        {
            string LogText = string.Format("{0}", string.Format(format));

            File.AppendAllText(LogFileName, LogText + Environment.NewLine);
        }
        public static void TextToSend(string format)
        {
            string LogText = string.Format("[{0}]: {1}", DateTime.Now.ToString("dd/MM/yy HH:mm:ss"), string.Format(format));

            File.AppendAllText(LogFileName, LogText);

            string LogTextToConsole = string.Format("{0}", string.Format(format));
            Console.Write(LogTextToConsole);
        }
    }
}

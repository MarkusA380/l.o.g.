﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace L.O.G.Server_Client
{
    class ClientSockets
    {
        public static void ReceivedBytes()
        {
            var Bytes = new byte[2048];
            int BytesRecevied = ClientConnections.ClientIP.Receive(Bytes, SocketFlags.None);

            if(BytesRecevied == 0)
            {
                return;
            }
            
            var DataRecevied = new byte[BytesRecevied];
            Array.Copy(Bytes, DataRecevied, BytesRecevied);
            string Text = Encoding.ASCII.GetString(DataRecevied);

            if (Text.ToLower() == "/quit")
            {
                ClientConnections.CloseAllConnections();
            }
        }
        public static void BytesToSend()
        {
            ClientLOG.TextToSend("You: ");
            string TextToSend = Console.ReadLine();
            ClientLOG.TextToLog(TextToSend);
            SendedBytes(TextToSend);

            if (TextToSend == "/quit")
            {
                ClientConnections.CloseAllConnections();
            }
        }
        public static void SendedBytes(string Text)
        {
            byte[] Bytes = Encoding.ASCII.GetBytes(Text);
            ClientConnections.ClientIP.Send(Bytes, 0, Bytes.Length, SocketFlags.None);
        }
    }
}

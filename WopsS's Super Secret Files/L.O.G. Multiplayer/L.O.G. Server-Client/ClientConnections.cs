﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace L.O.G.Server_Client
{
    class ClientConnections
    {
        public static readonly Socket ClientIP = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public static string ServerIP;
        public static string ServerPort;
        public static int ServerPortForConnect;
        public static void ConnectToServer()
        {
            ServerPortForConnect = Convert.ToInt32(ServerPort);

            int ConnectionAttempts = 0;
            bool ConectionTimeOut = false;
            while (!ClientIP.Connected && ConectionTimeOut == false)
            {
                try
                {
                    if (ConnectionAttempts == 0)
                    {
                        ConnectionAttempts++;
                        ClientLOG.LOGText("Connection attempt " + ConnectionAttempts + ".");
                        ClientIP.Connect(ServerIP, ServerPortForConnect);
                    }
                    else if (ConnectionAttempts == 5)
                    {
                        ConectionTimeOut = true;
                        System.Threading.Thread.Sleep(2000); // 2 Seconds to close.
                        ClientLOG.LOGText("Connection time out.");
                        ClientLOG.LOGText("-------------------------------------");
                        ClientLOG.LOGText("");
                        CloseAllConnections();
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(3000); // 3 Seconds to reconnect.
                        ConnectionAttempts++;
                        ClientLOG.LOGText("Connection attempt " + ConnectionAttempts + ".");
                        ClientIP.Connect(ServerIP, ServerPortForConnect);
                    }
                }
                catch (SocketException) { }
            }
            if (ClientIP.Connected)
            {
                Console.Clear();
                ClientLOG.LOGText("Connected to server.");
                ClientLOG.LOGText("-------------------------------------");
            }
            else
            {
                System.Threading.Thread.Sleep(1000); // 1 Second to close.
                Environment.Exit(0);
            }
        }
        public static void KeepConnected()
        {
            ClientLOG.LOGText("Type /quit to disconnect from the server.");

            while(true)
            {
                ClientSockets.BytesToSend();
                ClientSockets.ReceivedBytes();
            }
        }
        public static void CloseAllConnections()
        {
            ClientLOG.LOGText("");
            ClientLOG.LOGText("------------------Disconnect attempt at " + DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + "-------------------");
            ClientLOG.LOGText("Try to disconnect the server.");
            ClientIP.Shutdown(SocketShutdown.Both);
            ClientIP.Close();
            ClientLOG.LOGText("Disconnected.");
            ClientLOG.LOGText("---------------------------------------------------------------");

            Environment.Exit(0);
        }
    }
}

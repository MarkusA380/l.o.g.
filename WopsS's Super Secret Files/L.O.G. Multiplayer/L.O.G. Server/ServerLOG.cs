﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace L.O.G.Server
{
    class ServerLOG
    {
        private static string LogFileName = "server_log.txt";
        private static string BytesReceivedLog = "bytes_received_log.txt";
        private static string BytesSendedLog = "bytes_sended_log.txt";

        public static void CheckLogFile()
        {
            if (!System.IO.File.Exists(LogFileName))
            {
                using (System.IO.FileStream fs = System.IO.File.Create(LogFileName)) { }
            }
            if (!System.IO.File.Exists(BytesReceivedLog))
            {
                using (System.IO.FileStream fs = System.IO.File.Create(BytesReceivedLog)) { }
            }
            if (!System.IO.File.Exists(BytesSendedLog))
            {
                using (System.IO.FileStream fs = System.IO.File.Create(BytesSendedLog)) { }
            }
            if (System.IO.File.Exists(LogFileName))
            {
                TextToLog("");
                LOGText("-------------------------------------");
                LOGText("Log file (server_log.txt) loaded.");
                LOGText("-------------------------------------");
            }
        } 
        public static void WriteToLogFile(string format, params object[] argums)
        {

            string LogText = string.Format("[{0}]: {1}", DateTime.Now.ToString("dd/MM/yy HH:mm:ss"), string.Format(format, argums));
            string LogTextToConsole = string.Format("{0}", string.Format(format, argums));
            Console.WriteLine(LogTextToConsole);

            File.AppendAllText(LogFileName, LogText + Environment.NewLine);
        }
        public static void LOGText(string format, params object[] argums)
        {
            WriteToLogFile(format);
        }
        public static void TextToLog(string format, params object[] argums)
        {
            string LogText = string.Format("{0}", string.Format(format, argums));

            File.AppendAllText(LogFileName, LogText + Environment.NewLine);
        }
        public static void TextToSend(string format, params object[] argums)
        {
            string LogText = string.Format("[{0}]: {1}", DateTime.Now.ToString("dd/MM/yy HH:mm:ss"), string.Format(format, argums));

            File.AppendAllText(LogFileName, LogText);

            string LogTextToConsole = string.Format("{0}", string.Format(format));
            Console.Write(LogTextToConsole);
        }
        public static void BytesReceived(string format, params object[] argums)
        {
            string LogText = string.Format("[{0}] {1}", DateTime.Now.ToString("dd/MM/yy HH:mm:ss"), string.Format(format, argums));
            Console.Write(LogText);
            //File.AppendAllText(BytesReceivedLog, LogText + Environment.NewLine);
        }
        public static void BytesSended(string format, params object[] argums)
        {
            string LogText = string.Format("[{0}] {1}", DateTime.Now.ToString("dd/MM/yy HH:mm:ss"), string.Format(format, argums));
            Console.WriteLine(LogText);
            //File.AppendAllText(BytesSendedLog, LogText + Environment.NewLine);
        }
    }
}

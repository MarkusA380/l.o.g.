﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace L.O.G.Server
{
    public class ServerConnections
    {
        public static Socket Sockets;
        public const int Port = 4198;
        public static List<Socket> ListIP = new List<Socket>();

        public static void Connections()
        {

            ServerLOG.LOGText("-------------------------------------");
            Sockets = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            ServerLOG.LOGText("Socket created.");
            Sockets.Bind(new IPEndPoint(IPAddress.Any, Port));
            ServerLOG.LOGText("Socket binded.");
            Sockets.Listen(10);
            Sockets.BeginAccept(ServerSockets.AcceptSockets, null);
            ServerLOG.LOGText("-------------------------------------");

            ServerLOG.LOGText("Waiting for a connections ...");
            ServerLOG.LOGText("");
        }
        public static void KeepConnected()
        {
            ServerLOG.LOGText("Type \'help\' to show available commands.");

            while (true)
            {
                ServerSockets.BytesToRead();
            }
        }
        public static void CloseAllSockets()
        {
            ServerLOG.LOGText("");
            ServerLOG.LOGText("------------------Exit at " + DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + "-------------------");
            ServerLOG.LOGText("Try to close the sockets.");
            foreach (Socket Sockets in ServerSockets.ClientSockets)
            {
                try
                {
                    Sockets.Shutdown(SocketShutdown.Both);
                    Sockets.Close();
                }
                catch (SocketException)
                {
                    Sockets.Close();
                }
            }
            ServerLOG.LOGText("Socket closed.");

            ServerLOG.LOGText("Try to close the server.");
            ServerConnections.Sockets.Close();
            ServerLOG.LOGText("Server closed.");
            ServerLOG.LOGText("---------------------------------------------------------------");
            ServerLOG.LOGText("");
        }
    }
}

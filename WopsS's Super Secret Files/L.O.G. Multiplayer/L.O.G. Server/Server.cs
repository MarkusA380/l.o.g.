﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace L.O.G.Server
{
    class Server
    {
        public static void Main(string[] args)
        {
            Console.Title = "L.O.G. - Server";

            ServerLOG.TextToLog("");
            ServerLOG.LOGText("Server started at " + DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + ".");
            ServerLOG.CheckLogFile();
            ServerConnections.Connections();
            ServerConnections.KeepConnected();
        }
    }
}

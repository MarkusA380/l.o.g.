﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;


namespace L.O.G.Server
{
    class ServerSockets
    {
        public static readonly List<Socket> ClientSockets = new List<Socket>();
        public const int BytesSize = 16384;
        public static readonly byte[] Bytes = new byte[BytesSize];
        public static string[] IP = new string[9];
        public static int PlayersOnline = 0;

        public static string LocalIP;
        public static string[] ID = new string[9];
        public static int IDs = 0;

        public static int ReceivedSockets;
        public static string ReceivedText;

        public static Socket CurrentSockets;
        public static byte[] ToSendBytes;
        public static void AcceptSockets(IAsyncResult Results)
        {
            Socket Sockets;
            //string[] SocketIP;

            try
            {
                Sockets = ServerConnections.Sockets.EndAccept(Results);
            }
            catch (ObjectDisposedException) { return; }
            /*IDs = 0; 
            SocketIP = (Sockets.RemoteEndPoint.ToString()).Split(':');
            LocalIP = SocketIP[0];
            if (ID[IDs] != null)
            {
                IDs++;
            }
            else { 
                ID[IDs] = LocalIP;
            }
            IDs++;*/
            ClientSockets.Add(Sockets);
            Sockets.BeginReceive(Bytes, 0, BytesSize, SocketFlags.None, SocketsCallBack, Sockets);
            ServerLOG.LOGText("[Connection]: Incoming connection from " + Sockets.RemoteEndPoint);
            ServerConnections.Sockets.BeginAccept(AcceptSockets, null);
            ServerLOG.LOGText("[Connection]: Connection accepted from " + Sockets.RemoteEndPoint);
            PlayersOnline++;
        }
        public static void Received(int Client)
        {
            
        }
        public static void SocketsCallBack(IAsyncResult Results)
        {
            CurrentSockets = (Socket)Results.AsyncState;

            try
            {
                ReceivedSockets = CurrentSockets.EndReceive(Results);
            }
            catch (SocketException)
            {
                ServerLOG.LOGText("[Info]: Client " + CurrentSockets.RemoteEndPoint + " was forced disconnected.");
                CurrentSockets.Close();
                ClientSockets.Remove(CurrentSockets);
                PlayersOnline--;
                return;
            }
            byte[] ReceivedBytes = new byte[ReceivedSockets];
            Array.Copy(Bytes, ReceivedBytes, ReceivedSockets);
            ReceivedText = Encoding.ASCII.GetString(ReceivedBytes);
            ToSendBytes = Encoding.ASCII.GetBytes(PlayersOnline + "#" + CurrentSockets.RemoteEndPoint +" " + ReceivedText);
            ServerLOG.BytesReceived("Received: " + ToSendBytes);

            if (ReceivedText.ToLower() == "/quit")
            {
                ServerLOG.LOGText("[Info]: Client " + CurrentSockets.RemoteEndPoint + " disconnected");

                try
                {
                    CurrentSockets.Shutdown(SocketShutdown.Both);
                    CurrentSockets.Close();
                    ClientSockets.Remove(CurrentSockets);
                    PlayersOnline--;
                }
                catch (SocketException)
                {
                    ServerLOG.LOGText("[Info]: Client " + CurrentSockets.RemoteEndPoint + " was forced disconnected.");
                    CurrentSockets.Close();
                    ClientSockets.Remove(CurrentSockets);
                    PlayersOnline--;
                }

                return;
            }
            else
            {
                Console.WriteLine(PlayersOnline);
                foreach (var Clients in ClientSockets)
                {
                    try
                    {
                        Clients.Send(ToSendBytes);
                    }
                    catch (Exception) { }
                }
            }
            try
            {
                CurrentSockets.BeginReceive(Bytes, 0, BytesSize, SocketFlags.None, SocketsCallBack, CurrentSockets);
            }
            catch (SocketException)
            {
                ServerLOG.LOGText("[Info]: Client " + CurrentSockets.RemoteEndPoint + " was forced disconnected.");
                CurrentSockets.Close();
                ClientSockets.Remove(CurrentSockets);
                PlayersOnline--;
            }
        }
        public static void BytesToRead()
        {
            string TextToSend = Console.ReadLine();
            ServerLOG.TextToLog(TextToSend);

            if (TextToSend.ToLower() == "help")
            {
                ServerLOG.LOGText("");
                ServerLOG.LOGText("Available commands:");
                ServerLOG.LOGText("");
                ServerLOG.LOGText("clear - Clear all console log.");
                ServerLOG.LOGText("time - See current time on the server.");
                ServerLOG.LOGText("show ip - Show your actual IP.");
                ServerLOG.LOGText("exit - Stop the server.");
                ServerLOG.LOGText("");
            }
            else if (TextToSend.ToLower() == "say")
            {
                foreach (var Clients in ClientSockets)
                {
                    byte[] BytesToSend = Encoding.ASCII.GetBytes("test");
                    Clients.Send(BytesToSend);
                }
            }
            else if(TextToSend.ToLower() == "players online")
            {
                ServerLOG.LOGText("Number of online players is " + PlayersOnline + ".");
            }
            else if (TextToSend.ToLower() == "clear")
            {
                ServerLOG.LOGText("Console log will be clean.");
                System.Threading.Thread.Sleep(2000); // 2 Seconds to clear.
                Console.Clear();
            }
            else if (TextToSend.ToLower() == "time")
            {
                ServerLOG.LOGText("Time on server is: " + DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + ".");
            }
            else if (TextToSend.ToLower() == "show ip")
            {
                string IP = Dns.GetHostName();
                IPHostEntry IPEntry = Dns.GetHostEntry(IP);
                IPAddress[] IPAddress = IPEntry.AddressList;
                for (int i = 0; i < IPAddress.Length; i++)
                {
                    if (IPAddress[i].AddressFamily == AddressFamily.InterNetwork)
                        ServerLOG.LOGText("Server is binded for all IPs available, your IP is now: " + IPAddress[i] + ". This IP is for local machine.");
                }
            }
            else if (TextToSend.ToLower() == "exit")
            {
                ServerConnections.CloseAllSockets();
            }
            else
            {
                ServerLOG.LOGText("Invalid command, type \'help\' to see all available commands.");
            }
        }
        public static string BuildString(String Text, params object[] argums)
        {
            StringBuilder BuildRecevie = new StringBuilder();

            try
            {
                BuildRecevie.Append(Text + "$");
                return BuildRecevie.ToString();
            }
            catch (Exception e)
            {
                ServerLOG.LOGText("[ERROR]: " + e.StackTrace);
            }

            return "";
        }
    }
}
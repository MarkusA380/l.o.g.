<!DOCTYPE html>
<html>
  <head>
    <title>L.O.G. Multiplayer - Download</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="theme/css/bootstrap.min.css" rel="stylesheet">
	<link rel="icon" type="image/ico" href="favicon.ico" />
	<?php include_once("analyticstracking.php") ?>
	
  </head>
  <body>
  	<div class="container">
	    <div class="page-header">
			<a href="http://www.log-mp.com"><img src="theme/images/logo.png" /></a>
		</div>
		
			<!-- Bara de navigaie -->
	      <ul class="nav nav-pills nav-justified">
					<li><a href="index">Home</a></li>
					<li class="active"><a href="download">Download</a></li>
					<li><a href="media">Media</a></li>
					<li><a href="http://bugs.log-mp.com">Bugs</a></li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Others <span class="caret"></span></a>
							<ul class="dropdown-menu">
						    	<li><a href="team">Team</a></li>
						    	<li><a href="about">About</a></li>
						    	<li class="divider"></li>
						    	<li><a href="contact">Contact Us</a></li>
						    </ul>
					</li>
				</ul><br /><!--se inchide bara de navigatie -->
		<!-- Informatii de sus -->
		<div class="alert alert-success"><a href="http://www.log-mp.com" class="alert-link">L.O.G.</a> is an multiplayer extenstion for <a href="http://kerbalspaceprogram.com/" class="alert-link">Kerbal Space Program.</a></div>
		
	    <div class="alert alert-danger"><b>Sorry, we have not yet released the official version</b></div>
	    
	    <!-- Copyright -->
	    <div align="right">
			Copyright <span class="glyphicon glyphicon-copyright-mark"></span> 2013-2016 <a href="http://www.log-mp.com">L.O.G.</a> . All Rights Reserved. Released under <a href="http://www.gnu.org/licenses/gpl.html">GPLv3.</a><br />
			All images are copyrighted by their respective owners. <br />
			Build with <a href="http://getbootstrap.com">Bootstrap</a>. 
		</div>
	
    </div> <!-- /container -->




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="theme/js/bootstrap.min.js"></script>
  </body>
</html>

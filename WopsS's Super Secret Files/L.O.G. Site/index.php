<!DOCTYPE html>
<html>
  <head>
    <title>L.O.G. Multiplayer - Index</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="theme/css/bootstrap.min.css" rel="stylesheet">
    <script src="theme/js/jquery-1.10.2.min.js"></script>
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    <?php include_once("analyticstracking.php") ?>

  </head>
  <body>
  	<?php include_once("analyticstracking.php") ?>
  	<div class="container">
	    <div class="page-header">
			<a href="http://www.log-mp.com"><img src="theme/images/logo.png" /></a>
		</div>
		 <!-- Bara de navigaie -->
			<ul class="nav nav-pills nav-justified">
				<li class="active"><a href="index">Home</a></li>
				<li><a href="download">Download</a></li>
				<li><a href="media">Media</a></li>
				<li><a href="http://bugs.log-mp.com">Bugs</a></li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Others <span class="caret"></span></a>
						<ul class="dropdown-menu">
					    	<li><a href="team">Team</a></li>
					    	<li><a href="about">About</a></li>
					    	<li class="divider"></li>
					    	<li><a href="contact">Contact Us</a></li>
					    </ul>
				</li>
			</ul><br /><!--se inchide bara de navigatie -->
		<!-- Informatii de sus -->
		<div class="alert alert-success"><a href="http://www.log-mp.com" class="alert-link">L.O.G.</a> is an multiplayer extenstion for <a href="http://kerbalspaceprogram.com/" class="alert-link">Kerbal Space Program.</a></div>
		
	      <!-- Textul cel mare -->
		<div class="jumbotron">
			<h1>Initial Release</h1>
	        <p>The official launch of the plugin is not yet established because we want to offer you a pleasant game without bugs, crashes and others.</p>
	        <p>
	          <a class="btn btn-lg btn-primary" href="http://forum.kerbalspaceprogram.com/threads/35310-The-L-O-G-Multiplayer-Project" role="button">Go to the Forum</a>
	        </p>
			</div>
			
			<div class="jumbotron">
			<h1>New Video</h1>
	        <p>The new video from the project L.O.G.</p>
	        <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">Watch Video</button>

			<!-- Video -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">New Video</h4>
			      </div>
			      <div class="modal-body">	
			        <iframe width="540" height="405" src="//www.youtube.com/embed/rWYMXuc2XGI?rel=0" frameborder="0" allowfullscreen></iframe>
			       	<br /><br />
			       	 <div class="alert alert-warning" style="font-size: 12pt"><b>Note</b>: If you can not view the video activate the <a href="http://www.youtube.com/html5" class="alert-link">YouTube HTML5</a></div> 
			      </div>
			      
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
			      </div>
			    </div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			</div>
	      
		<!-- Copyright -->
		<div align="right">
			Copyright <span class="glyphicon glyphicon-copyright-mark"></span> 2013-2016 <a href="http://www.log-mp.com">L.O.G.</a> . All Rights Reserved. Released under <a href="http://www.gnu.org/licenses/gpl.html">GPLv3.</a><br />
			All images are copyrighted by their respective owners. <br />
			Build with <a href="http://getbootstrap.com">Bootstrap</a>. 
		</div>

    </div> <!-- /container -->




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="theme/js/bootstrap.min.js"></script>
  </body>
</html>

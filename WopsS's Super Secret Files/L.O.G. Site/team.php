<!DOCTYPE html>
<html>
  <head>
    <title>L.O.G. Multiplayer - Team</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="theme/css/bootstrap.min.css" rel="stylesheet">
    <script src="theme/js/jquery-1.10.2.min.js"></script>
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    <?php include_once("analyticstracking.php") ?>

  </head>
  <body>
  	<div class="container">
	    <div class="page-header">
			<a href="http://www.log-mp.com"><img src="theme/images/logo.png" /></a>
		</div>
		 <!-- Bara de navigaie -->
			<ul class="nav nav-pills nav-justified">
				<li><a href="index">Home</a></li>
				<li><a href="download">Download</a></li>
				<li><a href="media">Media</a></li>
				<li><a href="http://bugs.log-mp.com">Bugs</a></li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Others <span class="caret"></span></a>
						<ul class="dropdown-menu">
					    	<li class="active"><a href="team">Team</a></li>
					    	<li><a href="about">About</a></li>
					    	<li class="divider"></li>
					    	<li><a href="contact">Contact Us</a></li>
					    </ul>
				</li>
			</ul><br /><!--se inchide bara de navigatie -->
		<!-- Informatii de sus -->
		<div class="alert alert-success">Here is the <b>entire team</b> that helped create the plugin, <b>but not only.</b></div>
		
	      <!-- Tabel -->
		<table class="table table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Also Known As</th>
            <th>Role</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Markus</td>
            <td>Appel</td>
            <td><a href="http://forum.kerbalspaceprogram.com/members/46334-MarkusA380">MarkusA380</a></td>
            <td>Project Leader</td>
          </tr>
          <tr>
            <td>2</td>
            <td>-</td>
            <td>-</td>
            <td><a href="http://forum.kerbalspaceprogram.com/members/26366-pizzaoverhead">pizzaoverhead</a></td>
            <td>Project Advisor</td>
          </tr>
          <tr>
            <td>3</td>
            <td>-</td>
            <td>-</td>
            <td><a href="http://forum.kerbalspaceprogram.com/members/76686-zer0t3ch">zer0t3ch</a></td>
            <td>Plugin Development</td>
          </tr>
          <tr>
            <td>4</td>
            <td>Aaro</td>
            <td>Peramaa</td>
            <td><a href="http://forum.kerbalspaceprogram.com/members/63386-CARROTSAREAWSOME">CARROTSAREAWSOME</a></td>
            <td>Network Development</td>
          </tr>
          <tr>
            <td>5</td>
            <td>Bernhard</td>
            <td>-</td>
            <td><a href="http://forum.kerbalspaceprogram.com/members/33303-Tataffe">Tataffe</a></td>
            <td>Graphic Design</td>
          </tr>
          <tr>
            <td>6</td>
            <td>-</td>
            <td>-</td>
            <td><a href="http://forum.kerbalspaceprogram.com/members/40434-BlueSubstance">BlueSubstance</a></td>
            <td>Modelling</td>
          </tr>
          <tr>
            <td>7</td>
            <td>Dima</td>
            <td>Octavian</td>
            <td><a href="http://forum.kerbalspaceprogram.com/members/80580-WopsS">WopsS</a></td>
            <td>Website and Plugin developer</td>
          </tr>
        </tbody>
      </table>

	<br />
	      
		<!-- Copyright -->
		<div align="right">
			Copyright <span class="glyphicon glyphicon-copyright-mark"></span> 2013-2016 <a href="http://www.log-mp.com">L.O.G.</a> . All Rights Reserved. Released under <a href="http://www.gnu.org/licenses/gpl.html">GPLv3.</a><br />
			All images are copyrighted by their respective owners. <br />
			Build with <a href="http://getbootstrap.com">Bootstrap</a>. 
		</div>

    </div> <!-- /container -->




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="theme/js/bootstrap.min.js"></script>
  </body>
</html>

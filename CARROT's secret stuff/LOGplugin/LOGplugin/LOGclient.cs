﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using UnityEngine;

struct Player
{
    //public Vessel vessel = new Vessel();
    public float Xpos;
    public float Ypos;
    public float Zpos;
    public float Wrot;
    public float Xrot;
    public float Yrot;
    public float Zrot;

    public RDPartList Parts;

}


[KSPAddon(KSPAddon.Startup.Flight, false)]
public class LOGclient : MonoBehaviour
{
    protected Rect windowPos;

    //Plugin variables:
    bool connected = false;                     //Bool which difines whether the server is connected or not.
    public static string w_ip = "";             //The host IP
    string ip_edit;                             //The string that is written in the IP text field.
    public static string w_port = "";           //The host port
    string port_edit;                           //The string that is written in the port text field.
    public static string w_ownip = "";          //The own IP
    string ownip_edit;                          //The string that is written in the own IP text field.

    string path = "Ships\\VAB\\LOGrover.craft";
    public float SpawnHeightOffset = 1.0f;

    Vessel vessel;
    Vessel pl_vessel = new Vessel();
    Vector3 prebuild_pos = new Vector3(); //Just a save for the position of the Vessel vessel, so that it won't be shot to Jool when Build() happens. :D

    bool other_player = false;                  //Whether another player is on the server or not.

    //Client variables:
    string data = "";           //Data to be send
    string RecvIP = "";         //Recivied IP
    string superTemp = "";      //Temporary var. DONT TOUCH
    float RecvX = 0f;           //Recivied X pos
    float RecvY = 0f;           //Recivied Y pos
    float RecvZ = 0f;           //Recivied Z po
    float RecvWr = 0f;
    float RecvXr = 0f;
    float RecvYr = 0f;
    float RecvZr = 0f;
    List<string> RecvParts;

    String RecvMSG = "";        //Recivied message
    int num = 0;                //Temporary counter num
    string host;                //The host IP
    int port;                   //The host port
    public static List<string> players; //RANDOMMMMMMMMMMMMMMMMMMMMMMMMM
    public static int num2 = 1;
    TcpClient tcpclnt;

    public static float Wrot = 0f;
    public static float Xrot = 0f;
    public static float Yrot = 0f;
    public static float Zrot = 0f;
    public static RDPartList Parts;

    private void WindowGUI(int windowID) //The Graphical User Interface (GUI).
    {
        GUIStyle mySty = new GUIStyle(GUI.skin.button);
        mySty.normal.textColor = mySty.focused.textColor = Color.white;
        mySty.hover.textColor = mySty.active.textColor = Color.yellow;
        mySty.onNormal.textColor = mySty.onFocused.textColor = mySty.onHover.textColor = mySty.onActive.textColor = Color.green;
        mySty.padding = new RectOffset(2, 2, 2, 2);

        GUILayout.BeginVertical();

        if (connected == false)
        {
            GUILayout.Label("IP:", GUILayout.Width(90));
            ownip_edit = GUILayout.TextField(ownip_edit, 15, GUILayout.Width(90));
            GUILayout.Label("Host:", GUILayout.Width(90));
            ip_edit = GUILayout.TextField(ip_edit, 15, GUILayout.Width(90));
            GUILayout.Label("Port:", GUILayout.Width(90));
            port_edit = GUILayout.TextField(port_edit, 15, GUILayout.Width(90));


            if (GUILayout.Button("Connect", mySty, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
            {
                Connect();
                drawGUI(); //Refresh GUI
            }
        }
        else
        {
            if (GUILayout.Button("Disconnect", mySty, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
            {
                Disconnect();
                drawGUI(); //Refresh GUI

            }
        }

        GUILayout.EndVertical();
        GUI.DragWindow(new Rect(0, 0, 10000, 20));
    }

    private void drawGUI() //GUI draw function
    {
        GUI.skin = HighLogic.Skin;
        windowPos = GUILayout.Window(1, windowPos, WindowGUI, "L.O.G. plugin", GUILayout.MinWidth(100));
    }

    public void Start() //Called when plugin starts (like main)
    {
        windowPos = new Rect(Screen.width / 2, Screen.height / 2, 10, 10);
        RenderingManager.AddToPostDrawQueue(3, new Callback(drawGUI));//start the GUI

        ip_edit = string.Empty;
        port_edit = string.Empty;
        ownip_edit = string.Empty;

        IPHostEntry host; //Find clients own IP
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                ownip_edit = ip.ToString();
            }
        }
    }

    public void FixedUpdate() //Is called every Frame
    {

        RenderingManager.AddToPostDrawQueue(3, new Callback(drawGUI));
        vessel = FlightGlobals.ActiveVessel;
        pl_vessel = FlightGlobals.FindNearestControllableVessel(vessel);

        if (connected == true)
        {

            LOGclient.Wrot = FlightGlobals.ActiveVessel.transform.rotation.w;
            LOGclient.Xrot = FlightGlobals.ActiveVessel.transform.rotation.x;
            LOGclient.Yrot = FlightGlobals.ActiveVessel.transform.rotation.y;
            LOGclient.Zrot = FlightGlobals.ActiveVessel.transform.rotation.z;
            

            data = vessel.GetWorldPos3D().x + "_" + vessel.GetWorldPos3D().y + "_" + vessel.GetWorldPos3D().z + "_" + LOGclient.Wrot + "_" + LOGclient.Xrot + "_" + LOGclient.Yrot + "_" + LOGclient.Zrot + "_" + FlightGlobals.ActiveVessel.parts + "_";
            Stream stm = tcpclnt.GetStream(); //Start streaming data

            ASCIIEncoding asen = new ASCIIEncoding(); //Convert to ASCII
            byte[] ba = asen.GetBytes(data);
            stm.Write(ba, 0, ba.Length);
                                
            byte[] bb = new byte[100];
            int k = stm.Read(bb, 0, 100);
            
            for (int i = 0; i < k; i++)
            {

                RecvMSG = RecvMSG + (Convert.ToChar(bb[i])); //Decode recieved message

            }
            num2 = 0;
            while(RecvMSG[num2] != Convert.ToChar("$"))
            {
            
            #region
                num = 0 + num2;
                while (RecvMSG[num] != Convert.ToChar(" ")) //Figure IP out
                {

                    RecvIP = RecvIP + RecvMSG[num];
                    num = num + 1;

                }

                num = num + 1;
                while (RecvMSG[num] != Convert.ToChar("_"))  //Figure X out
                {
                    superTemp = superTemp + RecvMSG[num];
                    num = num + 1;
                }
                RecvX = Convert.ToSingle(superTemp);
                superTemp = "";

                num = num + 1;
                while (RecvMSG[num] != Convert.ToChar("_")) //Figure Y out
                {
                    superTemp = superTemp + RecvMSG[num];
                    num = num + 1;
                }
                RecvY = Convert.ToSingle(superTemp);
                superTemp = "";

                num = num + 1;
                while (RecvMSG[num] != Convert.ToChar("_")) //Figure Z out
                {
                    superTemp = superTemp + RecvMSG[num];
                    num = num + 1;
                }
                RecvZ = Convert.ToSingle(superTemp);
                superTemp = "";

                num = num + 1;
                while (RecvMSG[num] != Convert.ToChar("_")) //Figure Wrot out
                {
                    superTemp = superTemp + RecvMSG[num];
                    num = num + 1;
                }

                RecvWr = Convert.ToSingle(superTemp);

                superTemp = "";

                num = num + 1;
                while (RecvMSG[num] != Convert.ToChar("_")) //Figure Xrot out
                {
                    superTemp = superTemp + RecvMSG[num];
                    num = num + 1;
                }

                RecvXr = Convert.ToSingle(superTemp);

                superTemp = "";

                num = num + 1;
                while (RecvMSG[num] != Convert.ToChar("_")) //Figure Yrot out
                {
                    superTemp = superTemp + RecvMSG[num];
                    num = num + 1;
                }

                RecvYr = Convert.ToSingle(superTemp);

                superTemp = "";

                num = num + 1;
                while (RecvMSG[num] != Convert.ToChar("_")) //Figure Zrot out
                {
                    superTemp = superTemp + RecvMSG[num];
                    num = num + 1;
                }

                RecvZr = Convert.ToSingle(superTemp);
                
                superTemp = "";

                num = num + 1;


                while (RecvMSG[num] != Convert.ToChar("_")) //Figure Partslist out
                {
                    superTemp = superTemp + RecvMSG[num];
                    num = num + 1;
                }

                

                num = num + 1;
                num2 = num;

                superTemp = "";
                #endregion
            }
            if (RecvIP != w_ownip)
            {
               if (other_player == false)
               {
                   prebuild_pos = vessel.GetWorldPos3D();
                   Build();
                   vessel = FlightGlobals.ActiveVessel;
                   pl_vessel = FlightGlobals.FindNearestControllableVessel(vessel); //after building, the vessels have to be set new.
                   pl_vessel.SetPosition(new Vector3(10,10,10));
                   pl_vessel.SetWorldVelocity(new Vector3d(0, 0, 0));
                   vessel.SetPosition(prebuild_pos);
                   vessel.SetWorldVelocity(new Vector3d(0, 0, 0));
               }
               else
               {
                   pl_vessel.SetPosition(new Vector3(RecvX, RecvY, RecvZ));
                   print(RecvX + " " + RecvY + " " + RecvZ);
               }
           }
            if (other_player == true)
            {
                pl_vessel.SetWorldVelocity(new Vector3d(0, 0, 0));
            }
           

           RecvIP = "";
           RecvMSG = "";
           data = "";
           num = 0;
           data = "";
       }
        
    }

    public void Connect()
    {
        LOGclient.w_ip = ip_edit;
        LOGclient.w_port = port_edit;
        LOGclient.w_ownip = ownip_edit;
        connected = true;

        host = LOGclient.w_ip;
        port = Convert.ToInt32(w_port);
        tcpclnt = new TcpClient();

        print("Connecting....");

        tcpclnt.Connect(host, port); //Connect to host

        data = "";
        RecvIP = ""; //Assing some temporary variables
        superTemp = "";
        RecvX = 0f;
        RecvY = 0f;
        RecvZ = 0f;
        RecvMSG = "";
        num = 0;    
    }

    public void Disconnect()
    {
        connected = false;
        other_player = false;
        pl_vessel.DestroyVesselComponents();
        pl_vessel.Die();
        Destroy(pl_vessel);
        tcpclnt.Close(); //Close the connection    
    }

    private void Build()
    {
        ShipConstruct nship = ShipConstruction.LoadShip(path); //Loads a "Rover + Skycrane.craft"

        Vector3 offset = Vector3.up * SpawnHeightOffset;

        Transform t = vessel.transform;

        t.position = new Vector3(5f, 5f, 5f);

        string landedAt = "Launchpad";
        string flag = "default";
        Game state = FlightDriver.FlightStateCache;
        VesselCrewManifest crew = new VesselCrewManifest();

        GameObject launchPos = new GameObject();
        launchPos.transform.position = t.position;
        launchPos.transform.position += t.TransformDirection(offset);
        launchPos.transform.rotation = t.rotation;
        ShipConstruction.CreateBackup(nship);
        ShipConstruction.PutShipToGround(nship, launchPos.transform);
        Destroy(launchPos);
        ShipConstruction.AssembleForLaunch(nship, landedAt, flag, state, crew);
        FlightGlobals.SetActiveVessel(vessel);

        other_player = true;

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets; //Test
using System.IO;

using UnityEngine;

[KSPAddon(KSPAddon.Startup.Flight, false)]
public class LOGclient : MonoBehaviour
{
    protected Rect windowPos;

    //Plugin variables:
    bool connected = false;                     //Bool which difines whether the server is connected or not.
    public static string hostIp = "";             //The host IP
    string ip_edit;                             //The string that is written in the IP text field.
    public static string w_port = "";           //The host port
    string port_edit;                           //The string that is written in the port text field.
    public static string w_ownip = "";          //The own IP
    string ownip_edit;                          //The string that is written in the own IP text field.

    string path = "Ships\\VAB\\LOGrover.craft";
    public float SpawnHeightOffset = 1.0f;

    Vessel vessel;
    Vessel pl_vessel = new Vessel();
    Vector3 prebuild_pos = new Vector3(); //Just a save for the position of the Vessel vessel, so that it won't be shot to Jool when Build() happens. :D

    bool otherPlayerConnected = false;                  //Whether another player is on the server or not.

    //Client variables:
    public static string[] messageParts;
    int frameCount = 0;         //Frame counter
    int port;                   //The host port
    TcpClient tcpclnt;

    private void WindowGUI(int windowID) //The Graphical User Interface (GUI).
    {
        GUIStyle mySty = new GUIStyle(GUI.skin.button);
        mySty.normal.textColor = mySty.focused.textColor = Color.white;
        mySty.hover.textColor = mySty.active.textColor = Color.yellow;
        mySty.onNormal.textColor = mySty.onFocused.textColor = mySty.onHover.textColor = mySty.onActive.textColor = Color.green;
        mySty.padding = new RectOffset(2, 2, 2, 2);

        GUILayout.BeginVertical();

        if (connected == false)
        {
            GUILayout.Label("IP:", GUILayout.Width(90));
            ownip_edit = GUILayout.TextField(ownip_edit, 15, GUILayout.Width(90));
            GUILayout.Label("Host:", GUILayout.Width(90));
            ip_edit = GUILayout.TextField(ip_edit, 15, GUILayout.Width(90));
            GUILayout.Label("Port:", GUILayout.Width(90));
            port_edit = GUILayout.TextField(port_edit, 15, GUILayout.Width(90));


            if (GUILayout.Button("Connect", mySty, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
            {
                Connect();
                drawGUI(); //Refresh GUI
                
            }
            

        }
        else
        {
            

            if (GUILayout.Button("Disconnect", mySty, GUILayout.ExpandWidth(true)))//GUILayout.Button is "true" when clicked
            {
                Disconnect();
                drawGUI(); //Refresh GUI
                

            }
        }

        GUILayout.EndVertical();
        GUI.DragWindow(new Rect(0, 0, 10000, 20));
    }

    private void drawGUI() //GUI draw function
    {
        GUI.skin = HighLogic.Skin;
        windowPos = GUILayout.Window(1, windowPos, WindowGUI, "L.O.G. plugin", GUILayout.MinWidth(100));
        
    }

    public void Start() //Called when plugin starts (like main)
    {
        windowPos = new Rect(Screen.width / 2, Screen.height / 2, 10, 10);
        RenderingManager.AddToPostDrawQueue(3, new Callback(drawGUI));//start the GUI

        ip_edit = string.Empty;
        port_edit = string.Empty;
        ownip_edit = string.Empty;

        IPHostEntry host; //Find clients own IP
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                ownip_edit = ip.ToString();
            }
        }
    }

    public void Update() //Is called every Frame
    {
        //Debug.Log("#LOGclient 1");

        string vesselData = "";           //Data to be sent
        string RecvMsg = String.Empty;
        string RecvIP = "";         //Recivied IP
        float RecvX = 0f;           //Recivied X pos
        float RecvY = 0f;           //Recivied Y pos
        float RecvZ = 0f;           //Recivied Z pos
        float RecvRootX = 0f;
        float RecvRootY = 0f;
        float RecvRootZ = 0f;
        float RecvRotW = 0f;
        float RecvRotX = 0f;
        float RecvRotY = 0f;
        float RecvRotZ = 0f;
        
        

        //RenderingManager.AddToPostDrawQueue(3, new Callback(drawGUI));
        vessel = FlightGlobals.ActiveVessel;
        pl_vessel = FlightGlobals.FindNearestControllableVessel(vessel);

        //Debug.Log("#LOGclient 2");

        if (connected == true)
        {
            if (FlightGlobals.ActiveVessel == null)
            {
                Debug.LogError("LOGclient: Active vessel was null.");
                return;
            }
            vessel = FlightGlobals.ActiveVessel;

            pl_vessel = FlightGlobals.FindNearestControllableVessel(vessel);

            vesselData = VesselToMessage(vessel);
            Stream stream = tcpclnt.GetStream(); //Start streaming data

            byte[] ba = new ASCIIEncoding().GetBytes(vesselData);
            stream.Write(ba, 0, ba.Length);
            #region

            byte[] buffer = new byte[300];
            int k = stream.Read(buffer, 0, 300);
            //RecvMsg = System.Text.Encoding.ASCII.GetString(buffer, 0, buffer.Length);
            for (int i = 0; i < k; i++)
            {
                RecvMsg = RecvMsg + (Convert.ToChar(buffer[i])); //Decode recieved message
            }
            messageParts = RecvMsg.Split(' ');
            if (messageParts.Length != 11)
            {
                Debug.LogError("LOGclient: Received invalid message <" + RecvMsg + ">");
                return;
            }
                     
            //Debug.Log("#LOGclient 6");

                RecvIP = messageParts[0];
                Debug.Log("#LOGclient RecvIP: " + RecvIP);
                RecvX = (float)double.Parse(messageParts[1], System.Globalization.CultureInfo.InvariantCulture);
                RecvY = (float)double.Parse(messageParts[2], System.Globalization.CultureInfo.InvariantCulture);
                RecvZ = (float)double.Parse(messageParts[3], System.Globalization.CultureInfo.InvariantCulture);
                RecvRootX = (float)double.Parse(messageParts[4], System.Globalization.CultureInfo.InvariantCulture);
                RecvRootY = (float)double.Parse(messageParts[5], System.Globalization.CultureInfo.InvariantCulture);
                RecvRootZ = (float)double.Parse(messageParts[6], System.Globalization.CultureInfo.InvariantCulture);
                RecvRotX = (float)double.Parse(messageParts[7], System.Globalization.CultureInfo.InvariantCulture);
                RecvRotY = (float)double.Parse(messageParts[8], System.Globalization.CultureInfo.InvariantCulture);
                RecvRotZ = (float)double.Parse(messageParts[9], System.Globalization.CultureInfo.InvariantCulture);
                RecvRotW = (float)double.Parse(messageParts[10], System.Globalization.CultureInfo.InvariantCulture);

                //Debug.Log("#LOGclient 7");

                if (RecvIP != w_ownip)
                {
                    if (otherPlayerConnected == false)
                    {
                        prebuild_pos = vessel.GetWorldPos3D();
                        vessel.GoOnRails();
                        Build();
                        vessel.GoOffRails();
                        vessel = FlightGlobals.ActiveVessel;
                        pl_vessel = FlightGlobals.FindNearestControllableVessel(vessel); //after building, the vessels have to be set new.
                        pl_vessel.SetPosition(vessel.mainBody.position - new Vector3d(RecvRootX, RecvRootY, RecvRootZ) + new Vector3d(RecvX, RecvY, RecvZ));
                        pl_vessel.SetWorldVelocity(new Vector3d(0, 0, 0));
                        vessel.SetWorldVelocity(new Vector3d(0, 0, 0));
                        vessel.SetPosition(prebuild_pos);
                    }
                    else
                    {
                        pl_vessel.SetPosition(vessel.mainBody.position - new Vector3d(RecvRootX, RecvRootY, RecvRootZ) + new Vector3d(RecvX, RecvY, RecvZ));
                        print(RecvX + " " + RecvY + " " + RecvZ);
                        pl_vessel.SetRotation(new Quaternion(RecvRotX, RecvRotY, RecvRotZ, RecvRotW));
                    }
                }
            

            #endregion
            //Debug.Log("#LOGclient 8");



            if (otherPlayerConnected == true)
            {
                pl_vessel.SetWorldVelocity(new Vector3d(0, 0, 0));
            }
        }

    }

    /// <summary>
    /// Converts a vessel to a message for transmission.
    /// </summary>
    /// <param name="vessel"></param>
    /// <returns></returns>
    public string VesselToMessage(Vessel vessel)
    {
        try
        {
            // StringBuilder is faster than concatenation (+).
            StringBuilder sb = new StringBuilder();
            // Split over multiple lines so it fits on screen. Speed is the same.
            sb.Append(vessel.GetWorldPos3D().x.ToString()).Append(" ");
            sb.Append(vessel.GetWorldPos3D().y.ToString()).Append(" ");
            sb.Append(vessel.GetWorldPos3D().z.ToString()).Append(" ");
            sb.Append(FlightGlobals.ActiveVessel.mainBody.position.x.ToString()).Append(" ");
            sb.Append(FlightGlobals.ActiveVessel.mainBody.position.y.ToString()).Append(" ");
            sb.Append(FlightGlobals.ActiveVessel.mainBody.position.z.ToString()).Append(" ");
            sb.Append(vessel.transform.rotation.x.ToString()).Append(" ");
            sb.Append(vessel.transform.rotation.y.ToString()).Append(" ");
            sb.Append(vessel.transform.rotation.z.ToString()).Append(" ");
            sb.Append(vessel.transform.rotation.w.ToString());

            return sb.ToString();
        }
        catch (Exception ex)
        {
            Debug.LogError("LOGclient: " + ex.Message);
        }
        return "";
    }

    public void Connect()
    {
        LOGclient.hostIp = ip_edit;
        LOGclient.w_port = port_edit;
        LOGclient.w_ownip = ownip_edit;
        connected = true;

        string host = LOGclient.hostIp;
        port = Convert.ToInt32(w_port);
        tcpclnt = new TcpClient();

        Debug.Log("LOGclient: Connecting....");

        tcpclnt.Connect(host, port); //Connect to host
    }

    public void Disconnect()
    {
        connected = false;
        tcpclnt.Close(); //Close the connection    
    }

    private void Build()
    {
        ShipConstruct nship = ShipConstruction.LoadShip(path); //Loads a "Rover + Skycrane.craft"

        Vector3 offset = Vector3.up * SpawnHeightOffset;

        Transform t = vessel.transform;

        t.position = new Vector3(5f, 5f, 5f);

        string landedAt = "Launchpad";
        string flag = "default";
        Game state = FlightDriver.FlightStateCache;
        VesselCrewManifest crew = new VesselCrewManifest();

        GameObject launchPos = new GameObject();
        launchPos.transform.position = t.position;
        launchPos.transform.position += t.TransformDirection(offset);
        launchPos.transform.rotation = t.rotation;
        ShipConstruction.CreateBackup(nship);
        ShipConstruction.PutShipToGround(nship, launchPos.transform);
        Destroy(launchPos);
        ShipConstruction.AssembleForLaunch(nship, landedAt, flag, state, crew);
        FlightGlobals.SetActiveVessel(vessel);

        otherPlayerConnected = true;
    }
}
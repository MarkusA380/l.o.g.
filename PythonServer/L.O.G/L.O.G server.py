import socket
import time;
import sys
from _thread import *

HOST = ""   #Set the ip
PORT = int(input("Port: ")) #Define Port
getData = False;
tempConn = 0;
sleepTime = float(input("Update Timer (Sec)"));
MSG = "";
sendData = False;
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Create socket
print ("Socket created")
 
try:
    s.bind((HOST, PORT)) #Bind socket to Host and Port
except socket.error:
    print ("Bind failed. Error Code : " + str(msg[0]) + " Message " + msg[1])
    sys.exit()
     
print ("Socket bind complete")
 
s.listen(10)
print ("Socket now listening")
def datatimer(sleepTime): #This packs and sends the data
    global MSG;
    global getData;
    getData = False;
    updateCount = 0;
    while True:
        updateCount += 1;
        time.sleep(sleepTime / 3);
        getData = True;
        time.sleep(sleepTime / 3);
        getData = False;
        sendData = True;
        time.sleep(sleepTime / 3);
        sendData = False;
        print("Update (" + str(updateCount) + ")");
        MSG = "";

        
#Function for handling connections. This will be used to create threads
def clientthread(conn):
    global MSG
    global sendData
    global getData
    global addr
    hasSendData = False;
    hasGivedData = False;
    IP = addr[0]
    PRT = addr[1]
    finalData = "";
    #infinite loop so that function do not terminate and thread do not end.
    while True:
         
        #Receiving from client
        data = conn.recv(4096)
        reply = "§" + IP + " " + data.decode()
        if not data:
            break
        
        print (IP + ":" + str(PRT) + " sent data: " + data.decode())
        
        if(getData == True and hasGivedData == False):
            MSG = MSG + reply;
            hasGivedData = True;
        if(getData == False):
            hasGivedData = False;
            
        if(sendData == True and hasSendData == False):
            finalData = MSG + "$";
            conn.sendall(finalData.encode());
            hasSendData = True;
            
        if(sendData == False):
                         hasSendData = False;
        
    
    #came out of loop
    conn.close()
start_new_thread(datatimer ,(sleepTime,)) #Start the timer
#now keep talking with the client
while 1:
    #wait to accept a connection - blocking call
    conn, addr = s.accept()
    print ("Connected with " + addr[0] + ':' + str(addr[1]))
    IP = addr[0]
    PRT = addr[1]
    #start new thread
    start_new_thread(clientthread ,(conn,))

s.close()

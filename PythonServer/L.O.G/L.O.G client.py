#Socket client example in python
import time
import socket   #for sockets
import sys  #for exit
 
#create an INET, STREAMing socket
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print ("Failed to create socket")
    time.sleep(1)
    sys.exit()
     
print ("Socket Created")
 
host = input("Host: ") #Define Host
port = int(input("Port: ")) #Define Port
 
try:
    remote_ip = socket.gethostbyname( host ) #Figure out the hosts ip
 
except socket.gaierror:
    #could not resolve
    print ("Hostname could not be resolved. Exiting")
    time.sleep(1)
    sys.exit()
 
#Connect to remote server
s.connect((remote_ip , port))
print ("Socket Connected to " + host + " using ip " + remote_ip)


#Send some data to remote server
data = input("message: ")

#Send the massage
s.send(data.encode())
print ("data send successfully")

#Now receive data
reply = s.recv(4096)

print (reply)
time.sleep(5)

s.close()


